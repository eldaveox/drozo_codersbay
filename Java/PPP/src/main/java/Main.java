import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        /** ARRAY **/

       /* String[] fruit = {"Banana", "Pear", "Apple", "Strawberry"};
        System.out.println(fruit[2]);*/

        //----------------------------------------------------------------------------------------

        /** ARRAY LIST **/

        ArrayList<String> cars = new ArrayList<String>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");
        System.out.println("Arraylist " + cars);

        /*System.out.println(cars.get(3)); //nur 1 Element Anzeigen lassen

        //Wert vom ArrayList Element verändern
        cars.set(1, "Ferrari");
        System.out.println(cars.get(1));

        //Stringlänge
        System.out.println(cars.size());

        //Einzelner Element entfernen

        /*cars.remove(0);
        System.out.println(cars.get(0));*/


        //Alle Elemente entfernen
        /*cars.clear();
        System.out.println(cars);*/

    }
}
