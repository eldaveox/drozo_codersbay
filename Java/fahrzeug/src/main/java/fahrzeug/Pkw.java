package fahrzeug;

public class Pkw {
    private int id;
    private String marke ;
    private int baujahr;
    private int grundpreis;
    private int demoCarLastYear;
    private double rebatte;
    private int actualYear = 2021;
    private int pkwAge;
    private double rebattePerc = rebatte + (pkwAge * 0.03);
    private double finalPrice;

    public Pkw(int id, String marke, int baujahr, int grundpreis,int demoCarLastYear, double rebatte) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
        this.demoCarLastYear = demoCarLastYear;
        this.rebatte = rebatte;
        this.pkwAge = actualYear - baujahr;
    }




    private double rabatt(){

        double result = rebatte * pkwAge * (demoCarLastYear * 0.03);
        if (result > 0.15){
            result = 0.15;
        }
        return result;
    }

    public double finalPrice(){
        double result = grundpreis - (grundpreis * rabatt());
        return result;
    }

    /*______________________GETTER AND SETTER____________________________*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public int getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(int grundpreis) {
        this.grundpreis = grundpreis;
    }

    public int getDemoCarLastYear() {
        return demoCarLastYear;
    }

    public void setDemoCarLastYear(int demoCarLastYear) {
        this.demoCarLastYear = demoCarLastYear;
    }

    public double getRebatte() {
        return rebatte;
    }

    public void setRebatte(double rebatte) {
        this.rebatte = rebatte;
    }

    public int getActualYear() {
        return actualYear;
    }

    public void setActualYear(int actualYear) {
        this.actualYear = actualYear;
    }

    public int getPkwAge() {
        return pkwAge;
    }

    public void setPkwAge(int pkwAge) {
        this.pkwAge = pkwAge;
    }

    public double getRebattePerc() {
        return rebattePerc;
    }

    public void setRebattePerc(double rebattePerc) {
        this.rebattePerc = rebattePerc;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

   /* public List<Pkw> getPkws() {
        return pkws;
    }

    public void setPkws(List<Pkw> pkws) {
        this.pkws = pkws;
    }

    public Pkw getPkw1() {
        return pkw1;
    }

    public void setPkw1(Pkw pkw1) {
        this.pkw1 = pkw1;
    }

    public Pkw getPkw2() {
        return pkw2;
    }

    public void setPkw2(Pkw pkw2) {
        this.pkw2 = pkw2;
    }

    public Pkw getPkw3() {
        return pkw3;
    }

    public void setPkw3(Pkw pkw3) {
        this.pkw3 = pkw3;
    }*/







}
