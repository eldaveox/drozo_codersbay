package fahrzeug;

public class Lkw extends Fahrzeug {



    private int actualYear = 2021;
    private int lkwAge;
    private double percentage = 0.06;


    private double rebattePerc = 0.05 + (lkwAge * 0.03);
    private double finalPrice;

    public Lkw (int id, String marke, int baujahr, double grundpreis) {

        this.lkwAge = actualYear - baujahr;
    }

    public double getRabatt(){
        double result = lkwAge * percentage;
        if (result > 0.15) {
            result = 0.15;
        }
        return result;
    }


     public double getRabattLkw(double rebattePerc){
        if (rebattePerc > 0.15) {
            rebattePerc = 0.15;
        }
        return rebattePerc;
    }
    public double finalPriceLkw(){
        double result = Fahrzeug.getGrundpreis() - (Fahrzeug.getGrundpreis() * rebattePerc);
        return result;
    }

    //---------------------getter & setter -----------------------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(int grundpreis) {
        this.grundpreis = grundpreis;
    }

    public int getActualYear() {
        return actualYear;
    }

    public void setActualYear(int actualYear) {
        this.actualYear = actualYear;
    }

    public int getLkwAge() {
        return lkwAge;
    }

    public void setLkwAge(int lkwAge) {
        this.lkwAge = lkwAge;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public double getRebattePerc() {
        return rebattePerc;
    }

    public void setRebattePerc(double rebattePerc) {
        this.rebattePerc = rebattePerc;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    /*public List<Lkw> getLkws() {
        return lkws;
    }

    public void setLkws(List<Lkw> lkws) {
        this.lkws = lkws;
    }

    public Lkw getLkw1() {
        return lkw1;
    }

    public void setLkw1(Lkw lkw1) {
        this.lkw1 = lkw1;
    }

    public Lkw getLkw2() {
        return lkw2;
    }

    public void setLkw2(Lkw lkw2) {
        this.lkw2 = lkw2;
    }*/
}

