package fahrzeug;

public abstract class Fahrzeug {

    private int id;
    private String marke;
    private int baujahr;
    private double grundpreis;

    public Fahrzeug(int id, String marke, int baujahr, double grundpreis) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;


        public abstract void getRabatt();
        public abstract void getPreis(double rabatt);
        public abstract void print();

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }

