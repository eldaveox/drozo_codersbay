package autohaus;

import fahrzeug.Lkw;
import fahrzeug.Pkw;

import java.util.ArrayList;
import java.util.List;

public class Autohaus {

    private int amountParkingSpots;
    private int employee;
    private boolean overload;
    private String name = "Autohaus nice";

    public Autohaus() { }

    Lkw lkw1 = new Lkw(1,"Volvo", 2005, 12000);
    Lkw lkw2 = new Lkw(2,"VW", 2010, 15000);


    List<Lkw> lkws = new ArrayList<>(){
        {
            add(lkw1);
            add(lkw2);
        }
    };

    private Pkw pkw1 = new Pkw(1,"Audi",2019,2000,2021,0.0);
    private Pkw pkw2 = new Pkw(2,"Mercedes",1995,700,2004,0.0);
    private Pkw pkw3 = new Pkw(3,"VW",2015,4500, 2017,0.0);



    List<Pkw> pkws = new ArrayList<>(){
        {
            add(pkw1);
            add(pkw2);
            add(pkw3);
        }
    };

    public int amountCars(){
        return pkws.size() + lkws.size();
    }




    public void print(){
        System.out.println("~~~" + this.name + "~~~");
    }

    public boolean overloaded(){
        if(employee*3 < amountCars()){
            overload = false;
        }
        else{
            overload =true;
        }
        return overload;
    }

    public boolean overloaded(int amountParkingSpots, int amountVehicles, boolean overload){
        if(amountParkingSpots < amountVehicles){
            overload = true;
        }
        else{
            overload =false;
        }
        return overload;
    }



    /*-------- GETTER AND SETTER -------*/


    public int getAmountParkingSpots() {
        return amountParkingSpots;
    }

    public void setAmountParkingSpots(int amountParkingSpots) {
        this.amountParkingSpots = amountParkingSpots;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public boolean isOverload() {
        return overload;
    }

    public void setOverload(boolean overload) {
        this.overload = overload;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

