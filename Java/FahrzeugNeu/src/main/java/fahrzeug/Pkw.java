package fahrzeug;

public class Pkw extends Fahrzeug{

    private int serviceJahr;


    public Pkw(int id, String marke, int baujahr, double grundpreis, int serviceJahr) {
        super(id, marke, baujahr, grundpreis);
        this.serviceJahr=serviceJahr;
    }

    @Override
    public double getRabatt() {

    }

    @Override
    public double getPreis(double rabatt) {

    }

    @Override
    public void print() {

    }

    /*----------getter & setter-------------*/

    public int getServiceJahr() {
        return serviceJahr;
    }

    public void setServiceJahr(int serviceJahr) {
        this.serviceJahr = serviceJahr;
    }
}
