package fahrzeug;

public abstract class Fahrzeug {

    private int id;
    private String marke;
    private int baujahr;
    private double grundpreis;

    public Fahrzeug(int id, String marke, int baujahr, double grundpreis) {
        super();
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
    }

    public abstract double getRabatt();
    public abstract double getPreis (double rabatt);
    public abstract void print();


    /* --------getter & setter--------- */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }
}
