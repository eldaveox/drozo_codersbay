package test;

import java.util.Arrays;

interface Animal {
    public void animalSound(); // interface method (does not have a body)
}

class Dog implements Animal {
    public void animalSound() {
        System.out.println("The Dog says: Wuff Wuff");
    }
}


public class Test {

    public static void main(String[] args) {

            Dog myPig = new Dog();
            myPig.animalSound();
        }
}
