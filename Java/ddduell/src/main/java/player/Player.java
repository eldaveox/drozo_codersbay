package player;

import java.util.Scanner;

public class Player {

    Scanner scanner = new Scanner (System.in);
    private String player;
    private int playerHp;
    private String opponent = "DOLAN";

    public void playerSetup(){ //sets name and HP of the player(s)
        System.out.print("ENTER YOUR NAME: ");
        player = scanner.next();
        System.out.println("HI " + player + " YOU HAVE " + playerHp + " LIFE POINTS");
    }
    public void opponentSetup(){ //sets name and HP of the AI opponent
        System.out.println("YOUR OPPONENT " + opponent + " IS READY TO DUEL AND HE HAS " + playerHp  + " LIFE POINTS");
    }

    public void winLose(){
        if (playerHp == 0){
            System.out.println("YOU LOSE!!!");
        }
    }

    /*--------------getter & setter---------------*/

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getPlayerHp() {
        return playerHp;
    }

    public void setPlayerHp(int playerHp) {
        this.playerHp = playerHp;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }
}
