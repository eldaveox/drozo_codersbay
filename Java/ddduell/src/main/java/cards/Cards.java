package cards;

/** This shows the datatypes of the cards **/
public class Cards {

    private String name;
    private int atk;

    public Cards (String name, int atk) {
        this.name = name;
        this.atk = atk;
    }

    /*--------------getter & setter---------------*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }


}
