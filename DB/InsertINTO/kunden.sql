-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Jul 2021 um 12:55
-- Server-Version: 10.4.18-MariaDB
-- PHP-Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `test`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunden`
--

CREATE TABLE `kunden` (
  `kunden_id` int(11) NOT NULL,
  `nachname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `vorname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `land_id` int(11) DEFAULT NULL,
  `wohnort` varchar(255) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `kunden`
--

INSERT INTO `kunden` (`kunden_id`, `nachname`, `vorname`, `land_id`, `wohnort`) VALUES
(1, 'Schmitt', 'Heinrich', 2, 'Bielefeld'),
(2, 'Müller', 'Sabine', 2, 'Essen'),
(3, 'Mustermann', 'Markus', 1, 'Wien'),
(4, 'Maier', '', 0, ''),
(5, 'Bulgur', '', 0, 'Rheinfelden'),
(6, 'Manta', 'Maria', 0, ''),
(7, 'Fesenkampp', '', 3, 'Duisburg'),
(8, '', 'Herbert', 0, ''),
(9, 'Schulter', 'Albert', 1, 'Duisburg'),
(10, 'Sulcher', 'Brunhild', 1, 'Süderstade'),
(11, 'Soder', 'Hermann', 2, 'Bayersbronn'),
(12, 'Bursel', 'Aldi', 1, 'Emmendingen'),
(13, 'Bismarck', 'Heinrich', 1, 'Dortmund'),
(14, 'Güslick', 'Jocher', 2, 'Solingen'),
(15, 'Schmied', 'Jocher', 2, 'Solingen');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `kunden`
--
ALTER TABLE `kunden`
  ADD PRIMARY KEY (`kunden_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `kunden`
--
ALTER TABLE `kunden`
  MODIFY `kunden_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
