-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Okt 2013 um 17:46
-- Server Version: 5.5.32
-- PHP-Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `mycompany`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `EmployeeID` int(11) NOT NULL AUTO_INCREMENT,
  `LastName` varchar(20) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `FirstName` varchar(10) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `Title` varchar(30) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `TitleOfCourtesy` varchar(25) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `HireDate` date DEFAULT NULL,
  `Address` varchar(60) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `City` varchar(15) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `Region` varchar(15) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `PostalCode` varchar(10) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `Country` varchar(15) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `HomePhone` varchar(24) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `Extension` varchar(4) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`EmployeeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `employees`
--

INSERT INTO `employees` (`EmployeeID`, `LastName`, `FirstName`, `Title`, `TitleOfCourtesy`, `BirthDate`, `HireDate`, `Address`, `City`, `Region`, `PostalCode`, `Country`, `HomePhone`, `Extension`) VALUES
(1, 'Davolio', 'Nancy', 'Sales Representative', 'Ms.', '1968-12-08', '2003-05-01', '507 - 20th Ave. E.\r\nApt. 2A', 'Seattle', 'WA', '98122', 'United States', '(206) 555-9857', '5467'),
(2, 'Fuller', 'Andrew', 'Vice President, Sales', 'Dr.', '1952-02-19', '1992-08-14', '908 W. Capital Way', 'Tacoma', 'WA', '98401', 'United States', '(206) 555-9482', '3457'),
(3, 'Leverling', 'Janet', 'Sales Representative', 'Ms.', '1963-08-30', '2004-04-01', '722 Moss Bay Blvd.', 'Kirkland', 'WA', '98033', 'United States', '(206) 555-3412', '3355'),
(4, 'Peacock', 'Margaret', 'Sales Representative', 'Mrs.', '1958-12-19', '2013-05-03', '4110 Old Redmond Rd.', 'Redmond', 'WA', '98052', 'United States', '(206) 555-8122', '5176'),
(5, 'Buchanan', 'Steven', 'Sales Manager', 'Mr.', '1955-03-04', '1994-10-17', '14 Garrett Hill', 'London', '', 'SW1 8JR', 'United Kingdom', '(71) 555-4848', '3453'),
(6, 'Suyama', 'Michael', 'Sales Representative', 'Mr.', '1963-01-02', '1991-10-17', 'Coventry House\r\nMiner Rd.', 'London', '', 'EC2 7JR', 'United Kingdom', '(71) 555-7773', '428'),
(7, 'King', 'Robert', 'Sales Representative', 'Mr.', '1960-05-29', '1998-01-02', 'Edgeham Hollow\r\nWinchester Way', 'London', '', 'RG1 9SP', 'United Kingdom', '(71) 555-5598', '465'),
(8, 'Callahan', 'Laura', 'Inside Sales Coordinator', 'Ms.', '1958-01-09', '2004-03-05', '4726 - 11th Ave. N.E.', 'Seattle', 'WA', '98105', 'United States', '(206) 555-1189', '2344'),
(9, 'Dodsworth', 'Anne', 'Sales Representative', 'Ms.', '1969-07-02', '1995-11-15', '7 Houndstooth Rd.', 'London', '', 'WG2 7LT', 'United Kingdom', '(71) 555-4444', '452');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
