-- MariaDB dump 10.19  Distrib 10.4.18-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: uno
-- ------------------------------------------------------
-- Server version	10.4.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `uno`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `uno` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `uno`;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `continent` enum('Africa','Europe','Asia','North America','South America','Antarctica','Australia') NOT NULL,
  `capital` varchar(50) NOT NULL,
  `area` int(10) NOT NULL,
  `population` int(10) NOT NULL,
  `year_census` year(4) NOT NULL,
  `uno` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Colombia','South America','Bogota',1141748,50372424,2020,1),(2,'South Africa','Africa','Pretoria',1223037,59622350,2011,1),(3,'Russia','Asia','Moscow',17098246,143759445,2021,1),(4,'Afghanistan','Asia','Kabul',652230,32225560,2019,1);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professors`
--

DROP TABLE IF EXISTS `professors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professors` (
  `persnr` int(5) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `position` enum('c2','c3','c4') NOT NULL,
  `room` int(3) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`persnr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professors`
--

LOCK TABLES `professors` WRITE;
/*!40000 ALTER TABLE `professors` DISABLE KEYS */;
/*!40000 ALTER TABLE `professors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `test`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `test` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `test`;

--
-- Table structure for table `neuekunden`
--

DROP TABLE IF EXISTS `neuekunden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neuekunden` (
  `kunde_id` int(11) NOT NULL AUTO_INCREMENT,
  `nachname` varchar(255) DEFAULT NULL,
  `vorname` varchar(255) DEFAULT NULL,
  `land_id` int(11) DEFAULT NULL,
  `wohnort` varchar(255) DEFAULT NULL,
  `postleitzahl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kunde_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `neuekunden`
--

LOCK TABLES `neuekunden` WRITE;
/*!40000 ALTER TABLE `neuekunden` DISABLE KEYS */;
INSERT INTO `neuekunden` VALUES (1,'Schmitt','Heinrich',2,'Dortmund',NULL),(2,'Sabine Müller-Schmitt','Sabine',2,'Essen',NULL),(3,'Mussorgski','Michael',1,'Wien',NULL),(4,'Maier','Berta',1,'Stuttgart',NULL),(5,'Bulgur','Marianne',1,'Rheinfelden',NULL),(6,'Manta','Maria',NULL,NULL,NULL),(7,'Fesenkampp',NULL,3,'Duisburg',NULL),(8,NULL,'Herbert',NULL,NULL,NULL),(9,'Schulter','Albert',1,'Duisburg',NULL),(10,'Sulcher','Brunhild',1,'Süderstade',NULL),(11,'Soder','Hermann',2,'Bayersbronn',NULL),(12,'Bursel','Aldi',1,'Emmendingen',NULL),(13,'Bismarck','Heinrich',1,'Dortmund',NULL),(14,'Güslick','Jochen',2,'Solingen',NULL),(15,'Schmied','Jochen',2,'Solingen',NULL),(21,'Doppelbrecher','Siegfried',0,'',NULL),(23,'Schulter','Albert',1,'',NULL);
/*!40000 ALTER TABLE `neuekunden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schuhe`
--

DROP TABLE IF EXISTS `schuhe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schuhe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `groesse` varchar(4) NOT NULL,
  `preis` float NOT NULL,
  `farbe` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schuhe`
--

LOCK TABLES `schuhe` WRITE;
/*!40000 ALTER TABLE `schuhe` DISABLE KEYS */;
INSERT INTO `schuhe` VALUES (1,'Boots','42,5',99.9,'rot'),(2,'Outdoor King','XL',49,''),(3,'Outdoor Boss','',49.4,'grün');
/*!40000 ALTER TABLE `schuhe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-13 14:23:19
